import http.client
import json
import logging
import urllib.parse
from services.api.api import Api


log = logging.getLogger()


class PrettyShieldNoteRequest:
    def __init__(self, param_key: str, param_value: str, color: str, project_id: str, merge_req_id: str):
        self.param_key = param_key if param_key is not None else "default_key"
        self.param_value = param_value if param_value is not None else "default_value"
        self.color = color if color is not None else "orange"
        if len(project_id) == 0:
            raise Exception("project_id cannot be empty")
        self.project_id = project_id if project_id is not None else "default_project_id"
        self.merge_req_id = merge_req_id if merge_req_id is not None else "default_merge_id"

    def get_note(self):
        log.info(f"get_note w. param: key = {self.param_key}, value = {self.param_value}")
        parsed_key = urllib.parse.quote(self.param_key)
        parsed_val = urllib.parse.quote(self.param_value)
        img_url = f"https://img.shields.io/badge/{parsed_key}-{parsed_val}-{self.color}?style=flat"
        return img_url


class ShieldNoteService:
    def __init__(self, api: Api):
        self.api = api

    def create_note(self, create_note_req: PrettyShieldNoteRequest):
        log.info(f"create_note w. id: {create_note_req.project_id} & project_id: {create_note_req.merge_req_id}")
        gitlab_domain = self.api.domain if self.api.domain is not None else "gitlab.com"
        connection = http.client.HTTPSConnection(gitlab_domain)
        uri = f"/api/v4/projects/{create_note_req.project_id}/merge_requests/{create_note_req.merge_req_id}/notes"
        headers = {
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {self.api.api_key}'
        }
        body = {
            "id": int(create_note_req.project_id),
            "merge_request_iid": int(create_note_req.merge_req_id),
            "body": f"![image]({create_note_req.get_note()})"
        }
        content = json.dumps(body)
        connection.request("POST", uri, body=content, headers=headers)
        response = connection.getresponse()
        log.info(f"create_note http-status: {response.status}")

