FROM python:3.11.3-bullseye
WORKDIR /app
COPY . .
CMD ["python", "main.py"]