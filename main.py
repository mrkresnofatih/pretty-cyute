import logging
import os
from services.api.api import Api
from services.prettynotes.note import PrettyShieldNoteRequest, ShieldNoteService


log = logging.getLogger()
log.setLevel(logging.INFO)


def main():
    logging.info("Start PRETTY_CYUTE!")
    # token = "XXXXXXXXXXXXXXXX"
    token = os.getenv("PRETTY_CYUTE_TOKEN")
    # domain = "gitlab.com"
    domain = os.getenv("PRETTY_CYUTE_GITLAB_DOMAIN")
    # key = "UnitTest"
    key = os.getenv("PRETTY_CYUTE_KEY")
    # value = "Pass"
    value = os.getenv("PRETTY_CYUTE_VALUE")
    # color = "1abc9c"
    color = os.getenv("PRETTY_CYUTE_COLOR")
    # gitlab_project_id = "45780976"
    gitlab_project_id = os.getenv("PRETTY_CYUTE_PROJECT_ID")
    # gitlab_merge_request_id = "2"
    gitlab_merge_request_id = os.getenv("PRETTY_CYUTE_MERGE_REQ_ID")
    logging.info(f"exec w. tkn: {token[:3]}**************{token[-1]}")
    api = Api(
        api_key=token,
        domain=domain
    )
    create_req = PrettyShieldNoteRequest(
        param_key=key,
        param_value=value,
        color=color,
        project_id=gitlab_project_id,
        merge_req_id=gitlab_merge_request_id
    )
    notes_service = ShieldNoteService(
        api=api
    )
    notes_service.create_note(
        create_note_req=create_req
    )
    logging.info("Finish!")


if __name__ == '__main__':
    main()

